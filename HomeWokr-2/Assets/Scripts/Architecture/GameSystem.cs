using System;
using System.Collections.Generic;
using Architecture.Internal;
using GameManager;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Architecture
{
    public class GameSystem : MonoBehaviour
    {
        [ShowInInspector, ReadOnly]
        public GameStates GameStates => _gameMachine.GameStates;
        
        [SerializeField] private bool autorun;
        
        private readonly GameLocator _serviceLocator = new GameLocator();
        private readonly GameMachine _gameMachine = new GameMachine();
        private GameInstaller _gameInstaller;
        private readonly GameInjector _injector;
        
        private GameSystem()
        {
            _injector = new GameInjector(_serviceLocator);
        }
        
        private void Start()
        {
            InjectAll(transform);
            
            if (autorun)
            {
                InitGame();
                StartGame();
            }
        }

        public void AddListener(IGameListener listener)
        {
            _gameMachine.AddListener(listener);
        }

        public void RemoveListener(IGameListener listener)
        {
            _gameMachine.RemoveListener(listener);
        }
        
        public List<T> GetServices<T>()
        {
            return _serviceLocator.GetServices<T>();
        }
        
        public T GetService<T>()
        {
            return _serviceLocator.GetService<T>();
        }

        public object GetService(Type serviceType)
        {
            return _serviceLocator.GetService(serviceType);
        }

        public void AddService(object service)
        {
            _serviceLocator.AddService(service);
        }
        
        [Button]
        public void InitGame()
        {
            _gameMachine.InitGame();
        }
        
        [Button]
        public void StartGame()
        {
            _gameMachine.StartGame();
        }
        
        [Button]
        public void PauseGame()
        {
            _gameMachine.PauseGame();
        }

        [Button]
        public void ResumeGame()
        {
            _gameMachine.ResumeGame();
        }

        [Button]
        public void FinishGame()
        {
            Debug.Log("Game over!");
            Time.timeScale = 0;
            
            _gameMachine.FinishGame();
        }
        
        public void Update()
        {
            _gameMachine.Update();
        }

        public void FixedUpdate()
        {
            _gameMachine.FixedUpdate();
        }
        
        public void LateUpdate()
        {
            _gameMachine.LateUpdate();
        }

        private void InjectAll(Transform node)
        {
            var behaviours = node.GetComponents<MonoBehaviour>();

            foreach (var behaviour in behaviours)
            {
                Inject(behaviour);
            }

            foreach (Transform child in node)
            {
                InjectAll(child);
            }
        }
        
        private void Inject(object target)
        {
            _injector.Inject(target);
        }

    }
}