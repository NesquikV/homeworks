using GameManager;
using UnityEngine;

namespace Architecture
{
    public class GameInstaller : MonoBehaviour
    {
        [SerializeField] private MonoBehaviour[] services;
        
        private GameSystem _gameSystem;

        private void Awake()
        {
            _gameSystem = GetComponent<GameSystem>();
            
            InstallServices();
            ListenersInstaller();
        }

        private void InstallServices()
        {
            foreach (var service in services)
            {
                var componentServices = service.GetComponents<MonoBehaviour>();
                foreach (var componentService in componentServices)
                {
                    _gameSystem.AddService(componentService);
                }
            }
        }
        
        private void ListenersInstaller()
        {
            var listeners = GetComponentsInChildren<IGameListener>(true);

            foreach (var listener in listeners)
            {
                _gameSystem.AddListener(listener);
            }
        }
    }
}