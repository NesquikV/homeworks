using System.Collections.Generic;
using GameManager;
using UnityEngine;

namespace Architecture.Internal
{
    public class GameMachine
    {
        public GameStates GameStates => _gameStates;

        private GameStates _gameStates;
        
        private List<IGameListener> _listeners = new List<IGameListener>();

        private List<IGameUpdateListener> _updateListeners = new List<IGameUpdateListener>();
        private List<IGameFixedUpdateListener> _fixedUpdateListeners = new List<IGameFixedUpdateListener>();
        private List<IGameLateUpdateListener> _lateUpdateListeners = new List<IGameLateUpdateListener>();

        internal void AddListener(IGameListener listener)
        {
            _listeners.Add(listener);
            
            if (listener is IGameUpdateListener updateListener)
            {
                _updateListeners.Add(updateListener);
            }

            if (listener is IGameFixedUpdateListener fixedUpdateListener)
            {
                _fixedUpdateListeners.Add(fixedUpdateListener);
            }

            if (listener is IGameLateUpdateListener lateUpdateListener)
            {
                _lateUpdateListeners.Add(lateUpdateListener);
            }
            
            Debug.Log($"Add listener - {listener}");
        }
        
        internal void RemoveListener(IGameListener listener)
        {
            _listeners.Remove(listener);
            
            if (listener is IGameUpdateListener updateListener)
            {
                _updateListeners.Remove(updateListener);
            }

            if (listener is IGameFixedUpdateListener fixedUpdateListener)
            {
                _fixedUpdateListeners.Remove(fixedUpdateListener);
            }

            if (listener is IGameLateUpdateListener lateUpdateListener)
            {
                _lateUpdateListeners.Remove(lateUpdateListener);
            }
            
            Debug.Log($"Remove listener - {listener}");
        }
        
        internal void InitGame()
        {
            foreach (var listener in _listeners)
            {
                if (listener is IGameInitListener initListener)
                {
                    initListener.OnInitGame();
                }
            }

            _gameStates = GameStates.INIT;
            Debug.Log(_gameStates);
        }
        
        internal void StartGame()
        {
            if (_gameStates != GameStates.INIT)
            {
                return;
            }
            
            foreach (var listener in _listeners)
            {
                if (listener is IGameStartListener startListener)
                {
                    startListener.OnStartGame();
                }
            }
            
            _gameStates = GameStates.PLAY;
            Debug.Log(_gameStates);
        }

        internal void PauseGame()
        {
            if (_gameStates != GameStates.PLAY)
            {
                return;
            }
            
            foreach (var listener in _listeners)
            {
                if (listener is IGamePauseListener pauseListener)
                {
                    pauseListener.OnPauseGame();
                }
            }
            
            _gameStates = GameStates.PAUSE;
            Debug.Log(_gameStates);
        }

        internal void ResumeGame()
        {
            if (_gameStates != GameStates.PLAY)
            {
                return;
            }
            
            foreach (var listener in _listeners)
            {
                if (listener is IGameResumeListener resumeListener)
                {
                    resumeListener.OnResumeGame();
                }
            }
            
            _gameStates = GameStates.PLAY;
            Debug.Log(_gameStates);
        }

        internal void FinishGame()
        {
            if (_gameStates != GameStates.PLAY)
            {
                return;
            }
            
            foreach (var listener in _listeners)
            {
                if (listener is IGameFinishListener finishListener)
                {
                    finishListener.OnFinishGame();
                }
            }
            
            _gameStates = GameStates.FINISH;
            Debug.Log(_gameStates);
        }
        
        internal void Update()
        {
            if (_gameStates != GameStates.PLAY)
            {
                return;
            }
            
            var deltaTime = Time.deltaTime;
            for (var i = 0; i < _updateListeners.Count; i++)
            {
                var listener = _updateListeners[i];
                listener.OnUpdate(deltaTime);
            }
        }

        internal void FixedUpdate()
        {
            if (_gameStates != GameStates.PLAY)
            {
                return;
            }
            
            var fixedDeltaTime = Time.fixedDeltaTime;
            for (var i = 0; i < _fixedUpdateListeners.Count; i++)
            {
                var listener = _fixedUpdateListeners[i];
                listener.OnFixedUpdate(fixedDeltaTime);
            }
        }
        
        internal void LateUpdate()
        {
            if (_gameStates != GameStates.PLAY)
            {
                return;
            }
            
            var deltaTime = Time.deltaTime;
            for (var i = 0; i < _lateUpdateListeners.Count; i++)
            {
                var listener = _lateUpdateListeners[i];
                listener.OnLateUpdate(deltaTime);
            }
        }
    }
}