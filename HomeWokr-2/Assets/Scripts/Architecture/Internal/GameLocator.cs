using System;
using System.Collections.Generic;

namespace Architecture.Internal
{
    public class GameLocator
    {
        private List<object> _services = new List<object>();

        internal void AddService(object service)
        {
            _services.Add(service);
        }
        
        internal void RemoveServices(object service)
        {
            _services.Remove(service);
        }
        
        internal List<T> GetServices<T>()
        {
            var result = new List<T>();

            foreach (var service in _services)
            {
                if (service is T tService)
                {
                    result.Add(tService);
                }
            }

            return result; 
        }
        
        internal T GetService<T>()
        {
            foreach (var service in _services)
            {
                if (service is T result)
                {
                    return result;
                }
            }
            
            throw new Exception($"Services to type {typeof(T).Name} is not found");
        }

        internal object GetService(Type serviceType)
        {
            foreach (var service in _services)
            {
                if (serviceType.IsInstanceOfType(service))
                {
                    return service;
                }
            }
            
            throw new Exception($"Servie of type {serviceType.Name} is not found");
        }
    }
}