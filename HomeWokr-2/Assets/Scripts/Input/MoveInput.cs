using System;
using GameManager;
using UnityEngine;

namespace Input
{
    public sealed class MoveInput : MonoBehaviour, IGameUpdateListener
    {
        public Action<Vector2> OnMoveHorizontal;
         
        public void OnUpdate(float deltaTime)
        {
            Handle();
        }
        
        private void Handle()
        {
            if (UnityEngine.Input.GetKey(KeyCode.LeftArrow))
            {
                MoveHorizontal(-1);
            }
            else if (UnityEngine.Input.GetKey(KeyCode.RightArrow))
            {
                MoveHorizontal(1);
            }
            else
            {
                MoveHorizontal(0);
            }
        }

        private void MoveHorizontal(float directionHorizontal)
        {
            var direction = new Vector2(directionHorizontal,0f);
            OnMoveHorizontal?.Invoke(direction);
        }
    }
}