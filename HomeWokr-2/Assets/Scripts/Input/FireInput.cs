using System;
using GameManager;
using UnityEngine;

namespace Input
{
    public class FireInput : MonoBehaviour, IGameUpdateListener
    {
        public Action OnFire;
        
        public void OnUpdate(float deltaTime)
        {
            Handle();
        }
        
        private void Handle()
        {
            if (UnityEngine.Input.GetKeyDown(KeyCode.Space))
            {
                Fire();
            }
        }
        
        private void Fire()
        {
            OnFire?.Invoke();
        }
    }
}