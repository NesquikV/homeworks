using GameManager;
using UnityEngine;

namespace Level
{
    public sealed class LevelBackground : MonoBehaviour, IGameUpdateListener
    {
        [SerializeField] private LevelConfig levelConfig;
        [SerializeField] private Transform background;
        
        private float startPositionY;
        private float endPositionY;
        private float movingSpeedY;
        private float positionX;
        private float positionZ;

        private void Awake()
        {
            startPositionY = levelConfig.startPositionY;
            endPositionY = levelConfig.endPositionY;
            movingSpeedY = levelConfig.movingSpeedY;
            
            var position = transform.position;
            
            positionX = position.x;
            positionZ = position.z;
        }

        public void OnUpdate(float deltaTime)
        {
            MoveBackground();
        }
        
        private void MoveBackground()
        {
            if (background.transform.position.y <= endPositionY)
            {
                background.transform.position = new Vector3(
                    positionX,
                    startPositionY,
                    positionZ
                );
            }

            background.transform.position -= new Vector3(
                positionX,
                movingSpeedY * Time.fixedDeltaTime,
                positionZ
            );
        }
    }
}