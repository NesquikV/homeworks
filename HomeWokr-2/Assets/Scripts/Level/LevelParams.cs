using System;
using UnityEngine;

namespace Level
{
    [Serializable]
    public sealed class LevelConfig
    {
        [SerializeField] public float startPositionY;
        [SerializeField] public float endPositionY;
        [SerializeField] public float movingSpeedY;
    }
}