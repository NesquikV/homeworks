using System;
using DI;
using Enemy.Agents;
using UnityEngine;

namespace Enemy
{
    public class EnemySpawner : MonoBehaviour
    {
        public Action<Character.Character> OnEnemySpawned;
        
        [SerializeField] private Character.Character character;
        [SerializeField] private Transform worldTransform;
        
        private EnemyPositionsProvider _enemyPositionsProvider;
        
        [Inject]
        public void Construction(EnemyPositionsProvider enemyPositionsProvider)
        {
            _enemyPositionsProvider = enemyPositionsProvider;
        }
        
        public void SpawnEnemy(Character.Character enemy)
        {
            enemy.transform.SetParent(worldTransform);
            
            var spawnPosition = _enemyPositionsProvider.RandomSpawnPosition();
            enemy.transform.position = spawnPosition.position;
            
            var attackPosition = _enemyPositionsProvider.RandomAttackPosition();
            enemy.GetComponent<EnemyMoveAgent>().SetDestination(attackPosition.position);

            var attackAgent = enemy.GetComponent<EnemyAttackAgent>();
            attackAgent.SetTarget(character);

            OnEnemySpawned?.Invoke(enemy);
        } 
    }
}