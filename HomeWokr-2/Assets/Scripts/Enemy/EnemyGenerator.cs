using System.Collections;
using GameManager;
using UnityEngine;

namespace Enemy
{
    public class EnemyGenerator : MonoBehaviour, 
        IGameStartListener,
        IGameFinishListener
    {
        [SerializeField] private float timeSpawnNewEnemy = 1f;
        [SerializeField] private EnemySpawner _enemySpawner;
        
        private EnemyPool _enemyPool;
        private Coroutine _spawnCoroutine;

        public void OnStartGame()
        {
            _enemyPool = GetComponent<EnemyPool>();
            _spawnCoroutine = StartCoroutine(SpawnEnemy());
        }
        
        public void OnFinishGame()
        {
            StopCoroutine(_spawnCoroutine);
        }
        
        private IEnumerator SpawnEnemy()
        {
            while (true)
            {
                yield return new WaitForSeconds(timeSpawnNewEnemy);
                
                if (_enemyPool.TryGetFromPool(out var enemy))
                {
                    _enemySpawner.SpawnEnemy(enemy);
                }
            }
        }
    }
}