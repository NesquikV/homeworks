using System;
using GameManager;
using UnityEngine;

namespace Enemy.Agents
{
    public sealed class EnemyAttackAgent : MonoBehaviour, IGameFixedUpdateListener
    {
        public event Action<GameObject, Vector2, Vector2> OnFire;
        
        [SerializeField] private EnemyMoveAgent moveAgent;
        [SerializeField] private float countdown;

        private Character.Character _target;
        private float _currentTime;
        
        public void SetTarget(Character.Character target)
        {
            _target = target;
        }

        public void Reset()
        {
            _currentTime = countdown;
        }

        public void OnFixedUpdate(float deltaTime)
        {
            if (!moveAgent.IsReached || !_target)
            {
                return;
            }
            
            if (!_target.HitPointsComponent.IsHitPointsExists())
            {
                return;
            }

            _currentTime -= Time.fixedDeltaTime;

            if (!(_currentTime <= 0))
            {
                return;
            }
            
            Fire();
            _currentTime += countdown;
        }

        private void Fire()
        {
            var startPosition = GetComponent<Character.Character>().WeaponComponent.Position;
            var vector = (Vector2) _target.transform.position - startPosition;
            var direction = vector.normalized;
            
            OnFire?.Invoke(gameObject, startPosition, direction);
        }
    }
}