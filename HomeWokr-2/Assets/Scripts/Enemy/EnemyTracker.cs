using System.Collections.Generic;
using Bullets;
using DI;
using Enemy.Agents;
using GameManager;
using Level;
using UnityEngine;

namespace Enemy
{
    [RequireComponent(typeof(EnemyPool))]
    public sealed class EnemyTracker : MonoBehaviour, 
        IGameStartListener,
        IGameFixedUpdateListener
    {
        [SerializeField] private EnemySpawner enemySpawner;
        [SerializeField] private BulletConfig bulletConfig;
        
        private readonly HashSet<Character.Character> activeEnemies = new HashSet<Character.Character>();
        private readonly List<Character.Character> cache = new List<Character.Character>();
        
        private LevelBounds _levelBounds;
        private EnemyPool _enemyPool;
        private BulletSpawner _bulletSpawner;

        [Inject]
        public void Construction(LevelBounds levelBounds, BulletSpawner bulletSpawner)
        {
            _levelBounds = levelBounds;
            _bulletSpawner = bulletSpawner;
        }
        
        public void OnStartGame()
        {
            _enemyPool = GetComponent<EnemyPool>();
            enemySpawner.OnEnemySpawned += ActivateEnemy;
        }
        
        public void OnFixedUpdate(float deltaTime)
        {
            CheckPositionEnemy();
        }

        private void CheckPositionEnemy()
        {
            cache.Clear();
            cache.AddRange(activeEnemies);

            var cacheCount = cache.Count;
            for (var i = 0; i < cacheCount; i++)
            {
                var activeEnemy = cache[i];
                if (!_levelBounds.InBounds(activeEnemy.transform.position))
                {
                    OnDestroyed(activeEnemy);
                }
            }
        }
        
        private void ActivateEnemy(Character.Character enemy)
        {
            activeEnemies.Add(enemy);
            
            var attackAgent = enemy.GetComponent<EnemyAttackAgent>();
            
            enemy.HitPointsComponent.OnHpEmpty += OnDestroyed;
            attackAgent.OnFire += OnFire;
        }

        private void OnDestroyed(Character.Character enemy)
        {
            if (activeEnemies.Remove(enemy))
            {
                enemy.HitPointsComponent.OnHpEmpty -= OnDestroyed;
                enemy.GetComponent<EnemyAttackAgent>().OnFire -= OnFire;

                _enemyPool.SetToPool(enemy);
            }
        }

        private void OnFire(GameObject enemy, Vector2 position, Vector2 direction)
        {
            _bulletSpawner.CreateBullet(new BulletArgs
            {
                IsPlayer = false,
                PhysicsLayer = (int) bulletConfig.physicsLayer,
                Color = bulletConfig.color,
                Damage = bulletConfig.damage,
                Position = position,
                Velocity = direction * bulletConfig.speed
            });
        }
    }
}