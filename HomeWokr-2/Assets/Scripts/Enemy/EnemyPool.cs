using System.Collections.Generic;
using UnityEngine;

namespace Enemy
{
    public sealed class EnemyPool : MonoBehaviour
    {
        [Header("Pool")]
        [SerializeField, Range(1, 11)] private int poolSize = 7;
        [SerializeField] private Transform container;
        [SerializeField] private Character.Character prefab;

        private readonly Queue<Character.Character> enemyPool = new Queue<Character.Character>();
        
        public void Awake()
        {
            for (var i = 0; i < poolSize; i++)
            {
                var enemy = Instantiate(prefab, container);
                enemyPool.Enqueue(enemy);
            }
        }

        public bool TryGetFromPool(out Character.Character enemy)
        {
            var result = enemyPool.TryDequeue(out var character);
            enemy = character;
            
            return result;
        }

        public void SetToPool(Character.Character enemy)
        {
            enemy.transform.SetParent(container);
            enemyPool.Enqueue(enemy);
        }
    }
}