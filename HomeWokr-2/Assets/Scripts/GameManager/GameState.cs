namespace GameManager
{
    public enum GameStates
    {
        OFF = 0,
        INIT = 1,
        PLAY = 2,
        PAUSE = 3,
        RESUME = 4,
        FINISH = 5
    }
}