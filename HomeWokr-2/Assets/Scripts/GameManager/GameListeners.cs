namespace GameManager
{
    public interface IGameListener
    {
        
    }
    
    public interface IGameInitListener : IGameListener
    {
        public void OnInitGame();
    }
    
    public interface IGameStartListener : IGameListener
    {
        public void OnStartGame();
    }

    public interface IGamePauseListener : IGameListener
    {
        public void OnPauseGame();
    }

    public interface IGameResumeListener : IGameListener
    {
        public void OnResumeGame();
    }

    public interface IGameFinishListener : IGameListener
    {
        public void OnFinishGame();
    }
    
    public interface IGameUpdateListener : IGameListener
    {
        public void OnUpdate(float deltaTime);
    }

    public interface IGameFixedUpdateListener : IGameListener
    {
        public void OnFixedUpdate(float deltaTime);
    }

    public interface IGameLateUpdateListener : IGameListener
    {
        public void OnLateUpdate(float deltaTime);
    }
}