namespace Components
{
    public sealed class TeamComponent
    {
        public bool IsPlayer => _isPlayer;
        
        private bool _isPlayer;

        public TeamComponent(bool isPlayer)
        {
            _isPlayer = isPlayer;
        }
    }
}