using UnityEngine;

namespace Components
{
    public sealed class WeaponComponent
    {
        public Vector2 Position => _firePoint.position;
        public Quaternion Rotation => _firePoint.rotation; 

        private Transform _firePoint;

        public WeaponComponent(Transform firePoint)
        {
            _firePoint = firePoint;
        }
    }
}