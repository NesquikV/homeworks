using DI;
using Level;
using UnityEngine;

namespace Components
{
    public sealed class MoveComponent : MonoBehaviour
    {
        [SerializeField, Min(1f)] private float speed = 5.0f;
        
        [SerializeField] private new Rigidbody2D rigidbody2D;

        private LevelBounds _levelBounds;
        
        [Inject]
        public void Construction(LevelBounds levelBounds)
        {
            _levelBounds = levelBounds;
        }

        public void MoveByRigidbodyVelocity(Vector2 vector)
        {
            var nextPosition = rigidbody2D.position + vector * speed * Time.fixedDeltaTime;
            
            if (_levelBounds)
            {
                if (_levelBounds.InBounds(nextPosition))
                {
                    rigidbody2D.MovePosition(nextPosition);
                }
            }
            else
            {
                rigidbody2D.MovePosition(nextPosition);
            }
        }
    }
}