using System;

namespace Components
{
    public sealed class HitPointsComponent
    {
        public Action<Character.Character> OnHpEmpty;
        
        private int _hitPoints;
        private Character.Character _characterObject;

        public HitPointsComponent(int hitPoints, Character.Character characterObject)
        {
            _hitPoints = hitPoints;
            _characterObject = characterObject;
        }
        
        public bool IsHitPointsExists() 
        {
            return _hitPoints > 0;
        }

        public void TakeDamage(int damage)
        {
            _hitPoints -= damage;
            
            if (_hitPoints <= 0)
            {
                OnHpEmpty?.Invoke(_characterObject);
            }
        }
    }
}