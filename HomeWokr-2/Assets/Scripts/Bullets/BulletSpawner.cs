using System;
using UnityEngine;

namespace Bullets
{
    public class BulletSpawner : MonoBehaviour
    {
        public Action<Bullet> OnBulletSpawned;
        
        [SerializeField] private BulletPool pool;
        [SerializeField] private Transform worldTransform;
        
        public void CreateBullet(BulletArgs bulletArgs)
        {
            var bullet = pool.GetFromPool();
            
            bullet.transform.SetParent(worldTransform);
            bullet.SetPosition(bulletArgs.Position);
            bullet.SetColor(bulletArgs.Color);
            bullet.SetPhysicsLayer(bulletArgs.PhysicsLayer);
            bullet.SetDamage(bulletArgs.Damage);
            bullet.SetPlayer(bulletArgs.IsPlayer);
            bullet.SetVelocity(bulletArgs.Velocity);
            
            OnBulletSpawned?.Invoke(bullet);
        }
    }
}