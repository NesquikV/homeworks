using System.Collections.Generic;
using UnityEngine;

namespace Bullets
{
    public class BulletPool : MonoBehaviour
    {
        [SerializeField, Min(10)] private int initialCount = 50;
        
        [SerializeField] private Transform container;
        [SerializeField] private Bullet prefab;
        
        private readonly Queue<Bullet> bulletPool = new Queue<Bullet>();
        
        public void Awake()
        {
            for (var i = 0; i < initialCount; i++)
            {
                var bullet = Instantiate(prefab, container);
                bulletPool.Enqueue(bullet);
            }
        }

        public Bullet GetFromPool()
        {
            bulletPool.TryDequeue(out var bullet);
            return bullet;
        }
        
        public void SetToPool(Bullet bullet)
        {
            bullet.transform.SetParent(container);
            bulletPool.Enqueue(bullet);
        }
    }
}