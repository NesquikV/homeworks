using System.Collections.Generic;
using DI;
using GameManager;
using Level;
using UnityEngine;

namespace Bullets
{
    [RequireComponent(typeof(BulletPool))]
    public sealed class BulletTracker : MonoBehaviour, 
        IGameStartListener,
        IGameFinishListener,
        IGameFixedUpdateListener
    {
        [SerializeField] private BulletSpawner spawner;
        
        private BulletPool _bulletPool;
        private LevelBounds _levelBounds;
        private readonly HashSet<Bullet> activeBullets = new HashSet<Bullet>();
        private readonly List<Bullet> cache = new List<Bullet>();

        [Inject]
        public void Construction(LevelBounds levelBounds)
        {
            _levelBounds = levelBounds;
        }
        
        public void OnStartGame()
        {
            _bulletPool = GetComponent<BulletPool>();
            spawner.OnBulletSpawned += CreateBullet;
        }
        
        public void OnFinishGame()
        {
            spawner.OnBulletSpawned -= CreateBullet;
        }
        
        public void OnFixedUpdate(float deltaTime)
        {
            cache.Clear();
            cache.AddRange(activeBullets);

            var cacheCount = cache.Count;
            for (var i = 0; i < cacheCount; i++)
            {
                var bullet = cache[i];
                if (!_levelBounds.InBounds(bullet.transform.position))
                {
                    RemoveBullet(bullet);
                }
            }
        }

        private void CreateBullet(Bullet bullet)
        {
            if (activeBullets.Add(bullet))
            {
                bullet.OnCollisionEntered += OnBulletCollision;
            }
        }
        
        private void OnBulletCollision(Bullet bullet, GameObject objectCollision)
        {
            if (objectCollision.TryGetComponent(out Character.Character character))
            {
                if (bullet.IsPlayer == character.IsPlayer)
                {
                    return;
                }
                
                character.HitPointsComponent.TakeDamage(bullet.Damage);
            }
            
            RemoveBullet(bullet);
        }

        private void RemoveBullet(Bullet bullet)
        {
            if (activeBullets.Remove(bullet))
            {
                bullet.OnCollisionEntered -= OnBulletCollision;
                _bulletPool.SetToPool(bullet);
            }
        }
    }
}