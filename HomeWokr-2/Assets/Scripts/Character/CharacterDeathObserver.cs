using Architecture;
using GameManager;
using UnityEngine;

namespace Character
{
    public class CharacterDeathObserver : MonoBehaviour, IGameStartListener, IGameFinishListener
    {
        [SerializeField] private Character character; 
        [SerializeField] private GameSystem gameSystem;
        
        public void OnStartGame()
        {
            character.HitPointsComponent.OnHpEmpty += OnCharacterDeath;
        }

        public void OnFinishGame()
        {
            character.HitPointsComponent.OnHpEmpty -= OnCharacterDeath;
        }

        private void OnCharacterDeath(Character _)
        {
            gameSystem.FinishGame();
        }
    }
}