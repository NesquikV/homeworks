using Components;
using UnityEngine;

namespace Character
{
    public class Character: MonoBehaviour
    {
        public bool IsPlayer => _teamComponent.IsPlayer;
        public HitPointsComponent HitPointsComponent => _hitPointsComponent;
        public WeaponComponent WeaponComponent => _weaponComponent;
        
        [SerializeField] private int hitPoints;
        [SerializeField] private bool isPlayer;
        [SerializeField] private Transform firePoint;

        private HitPointsComponent _hitPointsComponent;
        private TeamComponent _teamComponent;
        private WeaponComponent _weaponComponent;
        
        private void Awake()
        {
            _hitPointsComponent = new HitPointsComponent(hitPoints, this);
            _teamComponent = new TeamComponent(isPlayer);
            _weaponComponent = new WeaponComponent(firePoint);
        }
    }
}