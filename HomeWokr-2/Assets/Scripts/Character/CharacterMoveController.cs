using Components;
using DI;
using GameManager;
using Input;
using UnityEngine;

namespace Character
{
    public class CharacterMoveController: MonoBehaviour, 
        IGameStartListener,
        IGameFinishListener
    {
        private MoveInput _input;
        private MoveComponent _characterMove;
        
        [Inject]
        public void Construction(MoveInput input, MoveComponent characterMove)
        {
            _input = input;
            _characterMove = characterMove;
        }
        
        void IGameStartListener.OnStartGame()
        {
            _input.OnMoveHorizontal += OnMoveCharacter;
        }

        void IGameFinishListener.OnFinishGame()
        {
            _input.OnMoveHorizontal -= OnMoveCharacter;
        }
        
        private void OnMoveCharacter(Vector2 direction)
        {
            _characterMove.MoveByRigidbodyVelocity(direction);
        }
    }
}