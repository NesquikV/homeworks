using Bullets;
using DI;
using GameManager;
using Input;
using UnityEngine;

namespace Character
{
    public class CharacterAttackController : MonoBehaviour, IGameStartListener, IGameFinishListener
    {
        [SerializeField] private Character character;
        [SerializeField] private BulletConfig bulletConfig;
        
        private FireInput _input;
        private BulletSpawner _bulletSpawner;
        
        [Inject]
        public void Construction(FireInput input, BulletSpawner bulletSpawner)
        {
            _input = input;
            _bulletSpawner = bulletSpawner;
        }
        
        public void OnStartGame()
        {
            _input.OnFire += OnFire;
        }

        public void OnFinishGame()
        {
            _input.OnFire -= OnFire;
        }
        
        private void OnFire()
        {
            var weapon = character.WeaponComponent;
            
            _bulletSpawner.CreateBullet(new BulletArgs
            {
                IsPlayer = true,
                PhysicsLayer = (int)bulletConfig.physicsLayer,
                Color = bulletConfig.color,
                Damage = bulletConfig.damage,
                Position = weapon.Position,
                Velocity = weapon.Rotation * Vector3.up * bulletConfig.speed
            });
        }
    }
}