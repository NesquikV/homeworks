using System;
using GameSystem;
using UnityEngine;

public class PermanentMove : MonoBehaviour, IGameUpdateListener
{
    [SerializeField] private Vector2 directionMove;
    
    public Action<Vector2> OnMove;
    
    private void Move(Vector2 direction)
    {
        OnMove?.Invoke(direction);
    }

    public void OnUpdate(float deltaTime)
    {
        Move(directionMove);
    }
}