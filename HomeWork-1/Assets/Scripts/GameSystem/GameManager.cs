using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameSystem
{
    public enum GameState
    {
        OFF = 0,
        START_NEW_GAME = 1,
        PLAY = 2,
        PAUSE = 3,
        FINISH = 4 
    }
    
    public class GameManager: MonoBehaviour
    {
        [ShowInInspector, ReadOnly]
        public GameState GameState => _gameState;

        private GameState _gameState;
        private readonly List<IGameListener> _listeners = new List<IGameListener>();
        private readonly List<IGameUpdateListener> _updateListeners = new List<IGameUpdateListener>();
        private readonly List<IGameFixedUpdate> _fixedUpdateListeners = new List<IGameFixedUpdate>();
        private readonly List<IGameLateUpdateListener> _lateUpdateListeners = new List<IGameLateUpdateListener>();
        
        private void Update()
        {
            if (_gameState != GameState.PLAY)
            {
                return;
            }
            
            var deltaTime = Time.deltaTime;
            for (var i = 0; i < _updateListeners.Count; i++)
            {
                var listener = _updateListeners[i];
                listener.OnUpdate(deltaTime);
            }
        }

        private void FixedUpdate()
        {
            if (_gameState != GameState.PLAY)
            {
                return;
            }

            var deltaTime = Time.fixedDeltaTime;
            for (var i = 0; i < _fixedUpdateListeners.Count; i++)
            {
                var listener = _fixedUpdateListeners[i];
                listener.OnFixedUpdate(deltaTime);
            }
        }

        private void LateUpdate()
        {
            if (_gameState != GameState.PLAY)
            {
                return;
            }
            
            var deltaTime = Time.deltaTime;
            for (var i = 0; i < _lateUpdateListeners.Count; i++)
            {
                var listener = _lateUpdateListeners[i];
                listener.OnLateUpdate(deltaTime);
            }
        }

        public void AddListener(IGameListener listener)
        {
            if (listener == null)
            {
                return;
            }
            
            _listeners.Add(listener);

            if (listener is IGameUpdateListener updateListener)
            {
                _updateListeners.Add(updateListener);
            }

            if (listener is IGameFixedUpdate fixedUpdateListener)
            {
                _fixedUpdateListeners.Add(fixedUpdateListener);
            }

            if (listener is IGameLateUpdateListener lateUpdateListener)
            {
                _lateUpdateListeners.Add(lateUpdateListener);
            }
        }

        public void StartNewGame()
        {
            foreach (var listener in _listeners)
            {
                if (listener is IGameStartListener initStartListener)
                {
                    initStartListener.OnCountDown();
                }
            }

            _gameState = GameState.START_NEW_GAME;
            Debug.Log(_gameState);
        }
        
        [Button]
        public void PlayGame()
        {
            if (_gameState != GameState.START_NEW_GAME)
            {
                return;
            }
            
            foreach (var listener in _listeners)
            {
                if (listener is IGamePlayListener playListener)
                {
                    playListener.OnStart();
                }
            }

            _gameState = GameState.PLAY;
            Debug.Log(_gameState);
        }

        [Button]
        public void PauseGame()
        {
            if (_gameState != GameState.PLAY)
            {
                return;
            }
            
            foreach (var listener in _listeners)
            {
                if (listener is IGamePauseListener pauseListener)
                {
                    pauseListener.OnPause();
                }
            }

            _gameState = GameState.PAUSE;
            Debug.Log(_gameState);
        }

        [Button]
        public void ResumeGame()
        {
            if (_gameState != GameState.PAUSE)
            {
                return;
            }
            
            foreach (var listener in _listeners)
            {
                if (listener is IGameResumeListener resumeListener)
                {
                    resumeListener.OnResume();
                }
            }

            _gameState = GameState.PLAY;
            Debug.Log(_gameState);
        }
        
        [Button]
        public void FinishGame()
        {
            foreach (var listener in _listeners)
            {
                if (listener is IGameFinishListener finishListener)
                {
                    finishListener.OnFinish();
                }
            }

            _gameState = GameState.FINISH;
            Debug.Log(_gameState);
        }
    }
}