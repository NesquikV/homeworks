namespace GameSystem
{
    public interface IGameListener
    {
        
    }

    public interface IGameStartListener : IGameListener
    {
        public void OnCountDown();
    }
    
    public interface IGamePlayListener: IGameListener
    {
        public void OnStart();
    }
    
    public interface IGamePauseListener: IGameListener
    {
        public void OnPause();
    }
    
    public interface IGameResumeListener: IGameListener
    {
        public void OnResume();
    }
    
    public interface IGameFinishListener: IGameListener
    {
        public void OnFinish();
    }

    public interface IGameUpdateListener: IGameListener
    {
        public void OnUpdate(float deltaTime);
    }

    public interface IGameFixedUpdate : IGameListener
    {
        public void OnFixedUpdate(float deltaTime);
    }
    
    public interface IGameLateUpdateListener : IGameListener
    {
        public void OnLateUpdate(float deltaTime);
    }
}