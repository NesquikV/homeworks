using GameSystem;
using UnityEngine;

public class MoveController : MonoBehaviour, IGamePlayListener, IGameFinishListener
{
    [SerializeField] private Player player;
    
    [SerializeField] private KeyboardInput input;
    [SerializeField] private PermanentMove permanentMove;

    private void OnInputMove(Vector2 direction)
    {
        player.ChangeRoad(direction.x);
    }

    private void OnPermanentMove(Vector2 direction)
    {
        player.Move(new Vector3(direction.x, 0, direction.y));
    }
    
    public void OnStart()
    {
        permanentMove.OnMove += OnPermanentMove;
        input.OnMove += OnInputMove;
    }

    public void OnFinish()
    {
        permanentMove.OnMove -= OnPermanentMove;
        input.OnMove -= OnInputMove;
    }
    
}