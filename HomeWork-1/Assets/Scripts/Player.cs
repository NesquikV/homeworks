using System;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private float speed;
   
    private int _numCurrentRoad;
    
    public Action OnCollision;
    
    public Vector3 GetPosition()
    {
        return transform.position;
    }

    public void ChangeRoad(float offset)
    {
        if (_numCurrentRoad == -1 && offset == -1 || _numCurrentRoad == 1 && offset == 1) 
            return;
        
        _numCurrentRoad += (int)offset;
        SetToRoad();
    }

    public void Move(Vector3 offset)
    {
        transform.position += offset * speed;
    }

    private void SetToRoad()
    {
        transform.position = new Vector3(_numCurrentRoad, transform.position.y, transform.position.z);
    }
    
    private void OnCollisionEnter(Collision other)
    {
        OnCollision?.Invoke();
    }
}