using GameSystem;
using UnityEngine;

public class CameraFollower : MonoBehaviour, IGameLateUpdateListener
    
{
    [SerializeField] private Camera targetCamera;
    [SerializeField] private Player player;
    [SerializeField] private Vector3 offset;

    public void OnLateUpdate(float deltaTime)
    {
        var position = player.GetPosition() + offset;
        targetCamera.transform.position = new Vector3(transform.position.x, position.y, position.z);;
    }
}
