using GameSystem;
using UnityEngine;

public class CollisionController : MonoBehaviour, IGamePlayListener, IGameFinishListener
{
    [SerializeField] private Player player;
    [SerializeField] private GameManager gameManager;

    public void OnStart()
    {
        player.OnCollision += GameOver;
    }

    public void OnFinish()
    {
        player.OnCollision -= GameOver;
    }
    
    private void GameOver()
    {
        gameManager.FinishGame();
    }
}