using GameSystem;
using UnityEngine;

public class ButtonsManager : MonoBehaviour, IGameStartListener, IGamePauseListener, IGameResumeListener, IGameFinishListener
{
    [SerializeField] private GameManager gameManager;
    
    [SerializeField] private GameObject startButton;
    [SerializeField] private GameObject pauseButton;

    public void StartGame()
    {
        if (gameManager.GameState == GameState.OFF)
            gameManager.StartNewGame();
        
        if (gameManager.GameState == GameState.PAUSE)
            gameManager.ResumeGame();
    }
    
    public void PauseGame()
    {
        if (gameManager.GameState == GameState.PLAY)
            gameManager.PauseGame();
    }

    public void OnCountDown()
    {
        SetPlayingModeButtons();
    }

    public void OnPause()
    {
        SetPausedModeButtons();
    }

    public void OnResume()
    {
        SetPlayingModeButtons();
    }

    public void OnFinish()
    {
        startButton.SetActive(false);
        pauseButton.SetActive(false);
    }
    
    private void SetPlayingModeButtons()
    {
        startButton.SetActive(false);
        pauseButton.SetActive(true);
    }

    private void SetPausedModeButtons()
    {
        startButton.SetActive(true);
        pauseButton.SetActive(false);
    }
}
