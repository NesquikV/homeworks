using System.Collections;
using GameSystem;
using TMPro;
using UnityEngine;

public class InitNewGameManager: MonoBehaviour, IGameStartListener
{
    [SerializeField] private GameManager gameManager;
    [SerializeField] private TMP_Text countDownText;
    
    public void OnCountDown()
    {
        countDownText.gameObject.SetActive(true);
        StartCoroutine(CountDown());
    }

    private IEnumerator CountDown()
    {
        SetCountDownText("3");
        yield return new WaitForSeconds(1);
        
        SetCountDownText("2");
        yield return new WaitForSeconds(1);
        
        SetCountDownText("1");
        yield return new WaitForSeconds(1);

        HideText();
        gameManager.PlayGame();
    }

    private void SetCountDownText(string text)
    {
        countDownText.text = text;
    }

    private void HideText()
    {
        countDownText.gameObject.SetActive(false);
    }
}