using System;
using GameSystem;
using UnityEngine;

public class KeyboardInput : MonoBehaviour, IGameUpdateListener
{
    public Action<Vector2> OnMove;

    private void HandleKeyboard()
    {
        if (Input.GetKeyUp(KeyCode.LeftArrow))
        {
            Move(Vector2.left);
        }

        if (Input.GetKeyUp(KeyCode.RightArrow))
        {
            Move(Vector2.right);
        }
    }

    private void Move(Vector2 direction)
    {
        OnMove?.Invoke(direction);
    }

    public void OnUpdate(float deltaTime)
    {
        HandleKeyboard();
    }
}